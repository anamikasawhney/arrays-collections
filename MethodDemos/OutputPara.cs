﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodDemos
{
    internal class OutputPara
    {
        //static int Add()
        //{

        //    return 100;
            
        //}
        // Output para are used when a function can return more than 1 value
        static void Operations(int num1, int num2,
            out int sum,
            out int difference,
            out int product,
            out float division)
        {
            sum =  num1 + num2;
            difference = num1 - num2;
            product = num1 * num2;
            division = num1 % num2;
        }
        static void Main()
        {
            // calling part
            int sum, subtarct, product;
            float rem;
            Operations(100, 25, out sum, out subtarct, out product, out rem);
            Console.WriteLine($"Sum is {sum}\n" +
                $"Difference is {subtarct}\n" +
                $"Product is {product}\n" +
                $"Remiander is {rem}"  );


        }
    }
}
