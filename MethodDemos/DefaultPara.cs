﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodDemos
{
    internal class DefaultPara
    {
        //static float SI( int rate, int principle = 20000,int time = 10)
        static float SI(int principle, int rate=7 , int time=10)
        {
            return (principle * rate * time) / 100;
        }
        static void Main()
        {
            Console.WriteLine(SI(12000, 3, 10));
            Console.WriteLine(SI(12000, 4));
            Console.WriteLine(SI(20000));
        }
    }
}
