﻿namespace MethodDemos
{
    internal class NamedParameter
    {
        static void DisplayDetails(int id,
            string name,
            string dept,
            float salary,
            DateTime Doj)
        {
            Console.WriteLine($"Id is {id}\n" +
                $"Name is {name}\n" +
                $"Dept is {dept}\n" +
                $"Salary is {salary}\n" +
                $"Date Of Joining is {Doj}"  );
        }
         static void Main(string[] args)
        {
            DisplayDetails(11, "ajay", "hr", 90909.3f, Convert.ToDateTime("12/12/2023"));

            DisplayDetails(id:90, name:"ajay", dept:"hr", salary: 90909.3f, Doj:Convert.ToDateTime("12/12/2023"));
            DisplayDetails(Doj: Convert.ToDateTime(DateTime.Now), name: "kapil",
                salary: 9090.9f, id: 900, dept: "accts");
        }
    }
}
