﻿namespace MethodDemos
{
    internal class MethodOverloading
    {
        //static int Add1(int x, int y)
        //{
        //    return x + y;
        //}

        //static int Add2(int x, int y, int z) {
        //    return x + y + z;
        //}

        //static float Add3(int x, float y)
        //{
        //    return x + y;
        //}

        //static string Add4(string s1, string  s2)
        //{
        //    //return s1 + " " + s2;
        //    return String.Concat(s1, " ", s2);
        //}

        static int Add(int x, int y)
        {
            return x + y;
        }

        static int Add(int x, int y, int z)
        {
            return x + y + z;
        }

        static float Add(int x, float y)
        {
            return x + y;
        }

        //static int Add(int x, float y)
        //{
        //    return x + y;
        //}

        static string Add(string s1, string s2)
        {
            //return s1 + " " + s2;
            return String.Concat(s1, " ", s2);
        }
        static void Main(string[] args)
        {
            //Console.WriteLine(Add1(10,20));
            //Console.WriteLine(Add2(2,3,4));
            Console.WriteLine(Add(1,2));
            Console.WriteLine(Add("ajay", "sood"));
        }
    }
}
