﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodDemos
{
    internal class ParamsArray
    {
        static int Add (params int[] num )
        {
            int sum = 0;
            foreach ( int i in num )
            {
                sum +=i;

            }
            return sum;

        }
        static void Main()
        {
            Console.WriteLine(Add(2,2,2,2));
            Console.WriteLine(Add(93,3,4,34,3,4,34,3,43,4,3,43,4,3434,34));
        }
    }
}
