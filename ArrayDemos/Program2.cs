﻿namespace ArrayDemos
{
    internal class Program2
    {
        static void Main(string[] args)
        {
            int[,] matrixA = new int[3, 3];
            Console.WriteLine("ENTER ELEMENETS");
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    matrixA[i, j] = Byte.Parse(Console.ReadLine());
                }
            }
            Console.WriteLine("Elements are ");
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < 3; j++)
                {
                    Console.Write(matrixA[i, j] + "\t");
                }
            }
            }
        }
}
