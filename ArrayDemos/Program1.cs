﻿namespace ArrayDemos
{
    internal class Program1
    {
        static void Main(string[] args)
        {
            // declare & initialize array
            int[] num = new int[] { 10, 20, 89, 27, 8, 120, 90, 86 };
            //Console.WriteLine("ENTER ELEMENETS");
            //    for(int i = 0;i<10;i++)
            //{
            //    num[i] = Byte.Parse(Console.ReadLine());
            //}
            Console.WriteLine("Elements are ");
            for (int i = 0; i < num.Length; i++)
            {
                Console.WriteLine(num[i]);
            }

            // When you want to iterate thru array or collection
            // we can use for loop and foreach loop/

            // for loop is used to repeat some statements
            // foreach loop is used to iterate thru an array or 
            // a collcetion

            // syntax for foreach loop
            // foreach(type range_varaible in array/collection)
            // access element thru range_varaible
            // 
            foreach(int temp in num)
                Console.WriteLine(temp);



            Console.WriteLine("Elements in reverse order are ");
            for (int i = num.Length-1; i >= 0; i--)
            {
                Console.WriteLine(num[i]);
            }
        }
    }
}
