﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
namespace CollectionsDemo
{
    internal class Program1
    {
        static void Main(string[] args)
        {
             
            //ArrayList list = new ArrayList();
            List<int> list = new List<int>();
            
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            //list.Add("ajay");
            //list.Add(90.8f);
            //list.Add("aaaaa");

          

            foreach (int i in list)
            {
                Console.WriteLine(i);
            }
            list.Add(100);
            list.Insert(1, 1000);
            list.Insert(4, 2000);
            Console.WriteLine("After isnertion");
            foreach (int i in list)
            {
                Console.WriteLine(i);
            }

            list.RemoveAt(0);
            list.Remove(1000);
            Console.WriteLine("After deletion");
            foreach (int i in list)
            {
                Console.WriteLine(i);
            }

            // When you want to have LIFO
            Stack<float> stack = new Stack<float>();
            stack.Push(1.9f);
            stack.Push(2.0f);
            stack.Push(78.7f);

            stack.Pop();
            Console.WriteLine("elements of stack");
            foreach (float i in stack)
            {
                Console.WriteLine(i);
            }


            // FIFO

            Queue<string> queue = new Queue<string>();
            queue.Enqueue("ajay");
            queue.Enqueue("deepak");
            queue.Enqueue("sagar");

            queue.Dequeue();

            // in all above cases, searching will be done sequentially
            foreach (int temp in list)
            {
                if (temp == 100)
                {
                    Console.WriteLine("FOND");
                        }
            }
            // WHEN WE WANT RANDOM SEARCHING, haSHING CUD BE USED
            ///*Hashtable*/ ht = new Hashtable();
            Dictionary<int, int> ht = new Dictionary<int, int>();
            // it stores values in <key,value> pair
            ht[10] = 90;
            ht[11] = 87;
            ht[100] = 88;
            ht[200] = 90;
            Console.WriteLine(ht[10]);
            if(ht.ContainsKey(11))
            {

            }

            foreach(int key in ht.Keys)
            {
                Console.WriteLine($"Scores of Rn {key} are {ht[key]}");
            }
        }

        static void PrintElements()
        {
            
        }
    }
}
