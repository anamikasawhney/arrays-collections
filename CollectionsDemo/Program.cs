﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
namespace CollectionsDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // collection is a data structure
            // it is dynamic in nature
            // its a general term
            // actual classes are arraylist, stack, queue, hashtable , etc
            ArrayList list = new ArrayList();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add("ajay");
            list.Add(90.8f);
            list.Add("aaaaa");

            //for(int i = 0;i< list.Count; i++) 
            //Console.WriteLine(list[i]);


            foreach (var i in list)
            {
                Console.WriteLine(i);
            }
            list.Add(100);
            list.Insert(1, 1000);
            list.Insert(4, 2000);
            Console.WriteLine("After isnertion");
            foreach (int i in list)
            {
                Console.WriteLine(i);
            }

            list.RemoveAt(0);
            list.Remove(1000);
            Console.WriteLine("After deletion");
            foreach (int i in list)
            {
                Console.WriteLine(i);
            }

            // When you want to have LIFO
            Stack stack = new Stack();
            stack.Push(1);
            stack.Push(2);
            stack.Push("aaa");

            stack.Pop();
            Console.WriteLine("elements of stack");
            foreach (var i in stack)
            {
                Console.WriteLine(i);
            }


            // FIFO

            Queue queue = new Queue();
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);

            queue.Dequeue();

            // in all above cases, searching will be done sequentially
            foreach (int temp in list)
            {
                if (temp == 100)
                {
                    Console.WriteLine("FOND");
                        }
            }
           // WHEN WE WANT RANDOM SEARCHING, haSHING CUD BE USED
           Hashtable ht = new Hashtable();
            // it stores values in <key,value> pair
            ht[10] = 90;
            ht[11] = 87;
            ht[100] = 88;
            ht["ajay"] = "delhi";
            Console.WriteLine(ht[10]);
            if(ht.ContainsKey(11))
            {

            }

            foreach(int key in ht.Keys)
            {
                Console.WriteLine($"Scores of Rn {key} are {ht[key]}");
            }
        }

        static void PrintElements()
        {
            
        }
    }
}
