﻿using System.Security.Cryptography;

namespace MiscTopics
{
    internal class Program1
    {
        //  both static and const variables are declared at class level
        // static vraoibales can be updated anywhere
        // const variables cannot be updated
        // once declared have to be initailzed also at time of declaration
        static int num1;
        const int num2 = 1000;
        static void Main(string[] args)
        {
            int num = 10;  // local variable
            // local variable > it is declared within a fucntion
            // ir a block. Its scope is that function or block only


            num1 = 100;
            Console.WriteLine(  num2);
            //num2 = 1000;// const variables caot be updated

        }
        static void Func2()
        {
        //num is not available here
            num1 = 300;
        }
    }
}
